const webpack = require('webpack');
const path = require('path');
const eslintFormatter = require('react-dev-utils/eslintFormatter');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
require('dotenv').config();
const { NODE_ENV,URL } = process.env;
module.exports = {
    performance: { hints: false },
    mode: NODE_ENV,
    // stats: 'errors-only',
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache:true,
                parallel:true,
                uglifyOptions: {
                    ecma: 8,
                    warnings: false,
                    compress: {
                        warnings: false,
                        properties: true,
                        sequences: true,
                        dead_code: true,
                        conditionals: true,
                        comparisons: true,
                        evaluate: true,
                        booleans: true,
                        unused: true,
                        loops: true,
                        hoist_funs: true,
                        if_return: true,
                        join_vars: true,
                        drop_console: true,
                        drop_debugger: true,
                        unsafe: false,
                        hoist_vars: true,
                        negate_iife: true,
                    },
                    output: {
                        comments: false
                    },
                    toplevel: false,
                    nameCache: null,
                    ie8: true,
                    keep_fnames: false
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ],
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    resolve: {
        alias: {
            "react-native/Libraries/Renderer/shims/ReactNativePropRegistry": "react-native-web/dist/modules/ReactNativePropRegistry",
            "react-native": "react-native-web"
        }
    },
    // devtool: 'eval',
    entry: {
        'app': [
            'babel-polyfill',
            path.resolve(__dirname, './react/src/index')
        ]
    },
    output: {
        path: path.join(__dirname, './public/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../'
                        }
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.(gif|svg|jpg|png|jpeg|eot|woff|ttf|woff2)$/,
                loader: "file-loader",
                options: {
                    limit: 10000,
                    name: '../images/[name].[ext]'
                },
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015', 'stage-0', 'react']
                        }
                    }
                ],
            },
            {
                test: /\.(js|jsx|mjs)$/,
                enforce: 'pre',
                exclude: /node_modules/,
                use: [
                    {
                        options: {
                            formatter: eslintFormatter,
                            eslintPath: require.resolve('eslint'),

                        },
                        loader: require.resolve('eslint-loader'),
                    },
                ],
                include: path.appSrc,
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(NODE_ENV)
            }
        }),

        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.AggressiveMergingPlugin(),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.js$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new MiniCssExtractPlugin({
            filename: "../css/[name].css",
            chunkFilename: "../css/[id].css"
        })
    ]
};
