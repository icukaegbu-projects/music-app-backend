'use strict';
/*=============================================================================*/
/**
 * Module dependencies
 */
//=============================================================================
const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate');

//=============================================================================
/**
 * User Schema
 */
//=============================================================================
const SongSchema = mongoose.Schema({
        song: {
            type: String
        },
        ownerId: {
            type: String
        },
        votes: {
            type: Number,
            default: 0
        },
        downloadCount: {
            type: Number,
            default: 0
        },
        artist:{
            type:String
        },
        producer: {
            type: String,
            default:"Active"
        }
    },
    {
        timestamps: true
    }
);
SongSchema.plugin(mongoosePaginate);
SongSchema.set('toJSON', {virtuals: true, getters: true});
SongSchema.set('toObject', {virtuals: true, getters: true});





//=============================================================================
/**
 * Compile to Model
 */
//=============================================================================
const SongModel = mongoose.model('Songs', SongSchema);
//=============================================================================
/**
 /**
 * Export Song Model
 */
//=============================================================================
module.exports = SongModel;
//=============================================================================
