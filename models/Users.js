'use strict';
//=============================================================================
/**
 * Module dependencies
 */
//=============================================================================
const mongoose = require('mongoose'),
     bcrypt = require('bcryptjs');
// console.log(process.env)
//=============================================================================
/**
 * User Schema
 */
//=============================================================================
const UserSchema = mongoose.Schema({
        first_name: {
            type: String
        },
        last_name: {
            type: String
        },
        password:{
             type:String
        },
        dob: {
            type: String
        },
        gender: {
            type: String
        },
        phone: {
            type: String
        },
        accountType: {
            type: String,
            default: 'user'
        },
        email: {
            type: String,
            index: { unique: true }
        },
        address: {
            type: String
        },
         status: {
           type: String,
           default:"Active"
         }
    },
    {
        timestamps: true
    }
    );
UserSchema.virtual('fullname').get(function () {
    return this.first_name + ' ' + this.last_name;
});
UserSchema.set('toJSON', {virtuals: true, getters: true});
UserSchema.set('toObject', {virtuals: true, getters: true});
UserSchema.methods.comparePassword = function comparePassword(password, callback) {
    bcrypt.compare(password, this.password, callback);
};


/**
 * The pre-save hook method.
 */
UserSchema.pre('save', function saveHook(next) {
    const user = this;

    // proceed further only if the password is modified or the user is new
    if (!user.isModified('password')) return next();


    return bcrypt.genSalt((saltError, salt) => {
        if (saltError) { return next(saltError); }

        return bcrypt.hash(user.password, salt, (hashError, hash) => {
            if (hashError) { return next(hashError); }

            // replace a password string with hash value
            user.password = hash;

            return next();
        });
    });
});



//=============================================================================
/**
 * Compile to Model
 */
//=============================================================================
const UsersModel = mongoose.model('users', UserSchema);
//=============================================================================
/**
/**
 * Export Users Model
 */
//=============================================================================
module.exports = UsersModel;
//=============================================================================
