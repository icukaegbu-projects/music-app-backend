'use strict';
//=============================================================================
/**
 * Module dependencies
 */
//=============================================================================
const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate');
//=============================================================================
/**
 * User Schema
 */
//=============================================================================
const BeatsSchema = mongoose.Schema({
        Beats: {
            type: String
        },
        ownerId: {
            type: String
        },
        downloadCount: {
            type: String
        },
        address: {
            type: String
        },
        artist:{
            type:String
        },
        producer: {
            type: String,
            default:"Active"
        }
    },
    {
        timestamps: true
    }
);
BeatsSchema.plugin(mongoosePaginate);
BeatsSchema.set('toJSON', {virtuals: true, getters: true});
BeatsSchema.set('toObject', {virtuals: true, getters: true});





//=============================================================================
/**
 * Compile to Model
 */
//=============================================================================
const BeatsModel = mongoose.model('Beats', BeatsSchema);
//=============================================================================
/**
 /**
 * Export Beats Model
 */
//=============================================================================
module.exports = BeatsModel;
//=============================================================================
