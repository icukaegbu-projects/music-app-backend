const {AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,OBS} = process.env,
 aws = require('aws-sdk'),
    spacesEndpoint = new aws.Endpoint(OBS),
    multer = require('multer'),
    multerS3 = require('multer-s3');
aws.config.update({
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    accessKeyId: AWS_ACCESS_KEY_ID,
    region: 'us-east-1'
});
const s3 = new aws.S3({
    endpoint: spacesEndpoint
});
const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'chike4nigeria',
        acl: 'public-read',
        key: function(request, file, ab_callback) {
            let newFileName = Date.now() + "-" + file.originalname;
            let fullPath = 'firstpart/'+ newFileName;
            ab_callback(null, fullPath);
        },
    })
}).array('upload', 1);
module.exports =upload;