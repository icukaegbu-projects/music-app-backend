const validator = require('validator');
/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateRegistrationForm(payload) {
    const errors = {};
    let isFormValid = true;
    let message = '';
    if (!payload || typeof payload.email !== 'string' || !validator.isEmail(payload.email)) {
        isFormValid = false;
        errors.email = 'Please provide a correct email address.'.toString();
    }
    if (!payload || typeof payload.password !== 'string' || payload.password.trim().length < 5) {
        isFormValid = false;
        errors.password = 'Password must have at least 8 characters.'.toString();
    }
    if (!payload || typeof payload.first_name !== 'string' || payload.first_name.trim().length === 0) {
        isFormValid = false;
        errors.first_name = 'Please provide your first name.'.toString();
    }
    if (!payload || typeof payload.last_name !== 'string' || payload.last_name.trim().length === 0) {
        isFormValid = false;
        errors.last_name = 'Please provide your last name.'.toString();
    }
    if (!isFormValid) {
        message = 'Check the form for errors.'.toString();
    }

    return {
        success: isFormValid,
        message,
        errors
    };
}
module.exports = validateRegistrationForm;