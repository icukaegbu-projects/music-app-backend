const {TITLE} = process.env;
const Beats = require('mongoose').model('Beats'),
    log = require('utils').logger.getLogger(TITLE);
module.exports =(req,res)=> {
    const query = {};
    const options = {
        sort: { createdAt: -1 },
        lean: true,
        limit: 10
    };
    Beats.paginate(query, options).then((result)=> {
        res.status(200).json(result)
    }).catch((err)=>{
        if(err){
            log.error('Error at Get All Beats route '+err);
            return res.status(400).json({
                success: false,
                message: 'Error loading Beats.'
            });
        }
    });
};