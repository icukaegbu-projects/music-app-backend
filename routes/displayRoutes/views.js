const express = require('express');
const view = express.Router();

/* GET home page. */
const {NODE_ENV} = process.env;
view.get('/', function(req, res, next) {
    res.render(NODE_ENV ==="development" ? "devIndex" :"index", { title: 'Chike4Nigeria' });
});

module.exports = view;