const voteSong = require('../Songs/voteSong');
const getAlSongs = require('../Songs/getAllSongs');
const SaveSong = require('../Songs/SaveSong');
const getAllBeats = require('../Beats/getAllBeats');
const express = require('express'),
    router = new express.Router();
//=================================
// Song route Section
    router.get('/allSong', getAlSongs);
    router.post('/saveSong', SaveSong);
    router.post('/voteSong', getAlSongs);
    router.post('/vote', getAlSongs);
//=================================
// Beat route Section
    router.get('/allBeats',getAllBeats);
    router.post('/saveBeat');
//=================================
module.exports = router;