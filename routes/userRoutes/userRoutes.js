const {TITLE} = process.env;
const express = require('express'),
    passport = require('passport'),
    log = require('utils').logger.getLogger(TITLE),
    registerValidation = require('../../tools/authValidation/registerValidation'),
    validateLoginForm = require('../../tools/authValidation/loginValidation'),
    router = new express.Router();
router.post('/register', (req, res, next) => {
    const validationResult = registerValidation(req.body);
    if (!validationResult.success) {
        return res.status(400).json({
            success: false,
            message: validationResult.message,
            errors: validationResult.errors
        });
    }
    return passport.authenticate('local-signup', (err) => {
        if (err) {
            if (err.name === 'MongoError' && err.code === 11000) {
                // the 11000 Mongo code is for a duplication email error
                // the 409 HTTP status code is for conflict error
                return res.status(409).json({
                    success: false,
                    message: 'Check the form for errors.',
                    errors: {
                        email: 'This email is already taken.'
                    }
                });
            }else {
                log.error('User registration error '+ err);
            }
            return res.status(400).json({
                success: false,
                message: 'Could not process the form.'
            });
        }

        return res.status(200).json({
            success: true,
            message: 'You have successfully signed up! Now you should be able to log in.'
        });
    })(req, res, next);
});
router.post('/login', (req, res, next) => {
    const validationResult = validateLoginForm(req.body);
    if (!validationResult.success) {
        return res.status(400).json({
            success: false,
            message: validationResult.message,
            errors: validationResult.errors
        });
    }
    return passport.authenticate('local-login', (err, token, userData) => {
        if (err) {
            if (err.name === 'IncorrectCredentialsError') {
                return res.status(400).json({
                    success: false,
                    message: err.message
                });
            }
            return res.status(400).json({
                success: false,
                message: 'Could not process.'
            });
        }
        return res.json({
            success: true,
            message: 'You have successfully logged in!',
            token,
            ...userData
        });
    })(req, res, next);
});

module.exports = router;