const {TITLE} = process.env;
const Song = require('mongoose').model('Songs'),
    log = require('utils').logger.getLogger(TITLE);
module.exports =(req,res)=> {
    const query = {};
    const options = {
        sort: { createdAt: -1 },
        lean: true,
        limit: 10
    };
    Song.paginate(query, options).then((result)=> {
        res.status(200).json(result)
    }).catch((err)=>{
        if(err){
            log.error('Error at Get All songs route '+err);
            return res.status(400).json({
                success: false,
                message: 'Error loading songs.'
            });
        }
    });
};