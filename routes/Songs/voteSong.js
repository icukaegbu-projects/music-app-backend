const {TITLE} = process.env;
const Song = require('mongoose').model('Songs'),
    log = require('utils').logger.getLogger(TITLE);
module.exports =(req,res)=> {
    Song.findByIdAndUpdate(req.body._id,
        {$inc: { votes: 1} },{lean: true,new: true, upsert: true}).then((result)=>{
        res.status(200).json(result)
    }).catch((err)=>{
        if(err){
            log.error('Error at vote Songs route '+err);
            return res.status(400).json({
                success: false,
                message: 'Error loading Songs.'
            });
        }
    });
};