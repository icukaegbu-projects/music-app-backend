const {TITLE} = process.env;
const Song = require('mongoose').model('Songs'),
    log = require('utils').logger.getLogger(TITLE),
    uploader =require('../../tools/fileUploader');
module.exports =(req,res)=> {
    uploader(req, res, function (error,da) {
        if (error) {
            console.log(error);
            return res.send("/error");
        }
        console.log('File uploaded successfully.',da);
        res.send("/success");
    });
};