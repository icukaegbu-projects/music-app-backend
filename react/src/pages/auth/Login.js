import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }
    handleChange=(e)=>{
        const {value,name} =e.target;
        this.setState({[name]:value},()=>console.log(this.state))
    };
    handleLogin=()=>{
        axios.post('/api/login',this.state)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    };
    render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }
      const { classes } = this.props;
      return (
          <div className={classes.root}>
              <Grid container>
                  <Grid item xs>
                  <div className={classes.loginFormHolder}>
                          <form onChange={this.handleChange} className={classes.container} noValidate autoComplete="off">
                              <TextField
                                  id="email"
                                  name="email"
                                  label="Email"
                                  className={classes.textField}
                                  value={this.state.name}
                                  margin="normal"
                              /><br/>
                              <TextField
                                  id="name"
                                  name="password"
                                  label="password"
                                  type="password"
                                  className={classes.textField}
                                  value={this.state.name}
                                  margin="normal"
                              />
                              <Button variant="contained" onClick={this.handleLogin} className={classes.button}>
                                  Default
                              </Button>
                          </form>
                  </div>
                  </Grid>
                  <Grid item xs={5}>
                      <div className={classes.loginImage}>
                      </div>
                  </Grid>

              </Grid>
          </div>
      );
  }
}
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    },
    loginFormHolder: {
        height: "100vh",
    },
    paper: {
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary,
        zIndex:-7777
    },
    loginImage: {
        height: "100vh",
        backgroundImage: `url(${require('../../Assets/images/candidate.jpg')})`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
    }
});


Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);