import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import LibraryMusic from '@material-ui/icons/LibraryMusic';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Person from '@material-ui/icons/Person';
import SurroundSound from '@material-ui/icons/SurroundSound';
import AudioList from '../containers/audio/audioList';
import MostRatedList from '../containers/MostRated/mostRatedList';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 80,
        width: 200,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
    },
    card: {
        display: 'flex',
    },
    mostRatedCard: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 89,
        height: 108,
        color:"#029c66"
    },
    mostRated: {
        padding: theme.spacing.unit * 2,
    },
    Top10: {
        padding: theme.spacing.unit * 2,
    },
});

class DashBoard extends React.Component {
    state = {};
    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.root} spacing={16}>
                <Grid item xs={12}>
                    <Grid
                        container
                        justify="space-around"
                        spacing={Number(16)}

                    >
                        <Grid item>
                            <Card className={classes.card}>
                                <Person
                                    className={classes.cover}
                                />
                                <div className={classes.details}>
                                    <CardContent className={classes.content}>
                                        <Typography variant="headline">Total users</Typography>
                                        <Typography variant="subheading" color="textSecondary">
                                            32434
                                        </Typography>
                                    </CardContent>
                                </div>
                            </Card>
                            </Grid>
                        <Grid item>
                            <Card className={classes.card}>
                                <SurroundSound
                                    className={classes.cover}
                                />
                                <div className={classes.details}>
                                    <CardContent className={classes.content}>
                                        <Typography variant="headline">Total Beats</Typography>
                                        <Typography variant="subheading" color="textSecondary">
                                            785
                                        </Typography>
                                    </CardContent>
                                </div>
                            </Card>
                            </Grid>
                        <Grid item>
                            <Card className={classes.card}>
                                <LibraryMusic
                                    className={classes.cover}
                                />
                                <div className={classes.details}>
                                    <CardContent className={classes.content}>
                                        <Typography variant="headline">Total Songs</Typography>
                                        <Typography variant="subheading" color="textSecondary">
                                            5643
                                        </Typography>
                                    </CardContent>
                                </div>
                            </Card>
                        </Grid>

                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.mostRated}>
                        <Grid container size={24}>
                            <Grid item xs={12}>
                                <Typography variant="subheading" component="h4" color="textSecondary">
                                    Most Rated
                                </Typography>
                                <Divider style={{marginBottom:14}}/>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container alignItems="center" justify="space-around" size={24}>
                                    <MostRatedList data={['a','c']}/>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.Top10}>
                        <Grid item xs={12}>
                            <Typography variant="subheading" component="h4" color="textSecondary">
                               Top 10 Songs
                            </Typography>
                            <Divider style={{marginBottom:14}}/>
                        </Grid>
                        <Grid spacing={16} container size={40}>
                            <AudioList data={['a','c','d','d','d','d','d']}/>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

DashBoard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashBoard);