import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Person from '@material-ui/icons/Person';
import LibraryMusic from '@material-ui/icons/LibraryMusic';
import Star from '@material-ui/icons/Star';
import SurroundSound from '@material-ui/icons/SurroundSound';
import Dashboard from '@material-ui/icons/Dashboard';
export const meanMenuListItems = (
    <div>
        <ListItem button>
            <ListItemIcon>
                <Dashboard />
            </ListItemIcon>
            <ListItemText primary="DashBoard" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <SurroundSound />
            </ListItemIcon>
            <ListItemText primary="Beats" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <LibraryMusic />
            </ListItemIcon>
            <ListItemText primary="Songs" />
        </ListItem>
        <ListItem button>
        <ListItemIcon>
            <Person />
        </ListItemIcon>
        <ListItemText primary="Users" />
    </ListItem>
        <ListItem button>
            <ListItemIcon>
                <Star/>
            </ListItemIcon>
            <ListItemText primary="Winners" />
        </ListItem>
    </div>
);