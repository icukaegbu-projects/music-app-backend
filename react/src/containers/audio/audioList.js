import React from 'react';
import AudioBox from './audioBox'
class AudioList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {hasError: false};
    }
    componentDidCatch(error, info) {
        // Display fallback UI
        this.setState({hasError: true});
        // You can also log the error to an error reporting service
        console.error(error, info);
    }
    render() {
        let {data} = this.props;
        let listData = data.map((data,index)=> <AudioBox key={index} {...data}/>);
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Something went wrong.</h1>;
        }
        return (listData);
    }
}
export default AudioList;
