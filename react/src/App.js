import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from 'react-router-dom'

import DashBoard from './containers/DashBoardContainer';
import LoginPage from './pages/auth/Login';
import Auth from './components/tools/Auth';

// remove tap delay, essential for MaterialUI to work properly
injectTapEventPlugin();

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        Auth.isUserAuthenticated() ? (
            <Component {...props} {...rest} />
        ) : (
            <Redirect to={{
                pathname: '/',
                state: { from: props.location }
            }}/>
        )
    )}/>
);
const PropsRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        <Component {...props} {...rest} />
    )}/>
);


class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: false
        }
    };

    componentDidMount() {
        // check if user is logged in on refresh
        this.toggleAuthenticateStatus()
    }

    toggleAuthenticateStatus() {
        // check authenticated status and toggle state based on that
        this.setState({ authenticated: Auth.isUserAuthenticated() })
    }

    render() {
        return (
            <Router>
                <div>
                <PropsRoute exact path="/" component={LoginPage} toggleAuthenticateStatus={() => this.toggleAuthenticateStatus()} />
                <PrivateRoute path="/dashboard" component={DashBoard}/>
                </div>
            </Router>
        );
    }
}

export default Main;
