'use strict';
// if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'test') {
//     require('dotenv').config();
// }
//=============================================================================
/**
 * Module dependencies
 */
//=============================================================================
const {TITLE,OBS} = process.env;
const
    express = require('express'),
    bParser = require('body-parser'),
    mongoose = require('mongoose'),
    log4js=require('log4js'),
    path = require('path'),
    gzipAssets = require('./middleware/config/gzipAssets'),
    webPackServerConfig = require('./middleware/config/webpackServerConfig'),
    createError = require('http-errors'),
    cookieParser = require('cookie-parser'),
    fs = require('fs'),
    passport = require('passport'),
    log = require('utils').logger.getLogger(TITLE);
    mongoose.Promise = require('bluebird');


//=============================================================================
/**
 * express instance
 */
//=============================================================================
const app = express();
app.disable('x-powered-by');
//=============================================================================
/**
 * Module variables
 */
//=============================================================================
const
    port = process.env.PORT,
    env = process.env.NODE_ENV,
    DBURL = process.env.DBURL;
let db;

//=============================================================================
/**
 * App config
 */
//=============================================================================
app.set('port', port);
app.set('env', env);

//=============================================================================
/**
 * Route files
 */
//=============================================================================
const userRoutes = require('./routes/userRoutes/userRoutes');
const views = require('./routes/displayRoutes/views');
//=============================================================================
/**
 * database config
 */
//=============================================================================


let promiselib = require('bluebird');
promiselib.config({
    warnings: true,
    longStackTraces: true,
    cancellation: true,
    monitoring: true
});

let options = {promiseLibrary: promiselib,useMongoClient: true, keepAlive: 1, connectTimeoutMS: 30000};

mongoose.connect(DBURL, options);

db = mongoose.connection;
db.on('error', err => {
    log.error('There was a db connection error');
});
db.once('connected', () => {
    log.info('Successfully connected to ' + DBURL);
});
db.once('disconnected', () => {
    log.error('Successfully disconnected from ' + DBURL);
});
process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        log.error('dBase connection closed due to app termination');
        process.exit(0);
    });
});

//=================================================================
// All mongoose model will be imported here automatically
const models_path = __dirname + '/models';
fs.readdirSync(models_path).forEach(function (file) {
    if (~file.indexOf('.js')) require(models_path + '/' + file)
});
const appRoutes = require('./routes/userRoutes/routes');
//=============================================================================
/**
 * Module middleware
 */
//=============================================================================
// webPackServerConfig(app);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(log4js.connectLogger(log, {level: 'auto'}));
app.use(bParser.json({limit: '50mb'}));
app.use(bParser.urlencoded({limit: '50mb', extended: true}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.status(200).end();
    }
    else {
        next();
    }
});
app.use(passport.initialize());
app.use(gzipAssets);
app.use(express.static(path.join(__dirname, 'public')));
require('./passport/passport');

//=============================================================================
/**
 * Routes
 */
//=============================================================================
app.get('/test', (req, res) => {
    return res.status(200).json('OK');
});

app.use('/',views);
app.use('/api/user',userRoutes);
app.use('/api/user',appRoutes);
app.use('/api/admin',userRoutes);
// app.use('/api',routes);

app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? {} : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
//=============================================================================
/**
 * Export app
 */
//=============================================================================
module.exports = app;
//=============================================================================
