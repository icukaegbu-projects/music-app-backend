const webpack = require('webpack');
const path = require('path');
const eslintFormatter = require('react-dev-utils/eslintFormatter');

module.exports = {
    performance: { hints: false },
    devtool: 'source-map',
    resolve: {
        alias: {
            "react-native/Libraries/Renderer/shims/ReactNativePropRegistry": "react-native-web/dist/modules/ReactNativePropRegistry",
            "react-native": "react-native-web"
        }
    },
    mode: 'development',
    entry: {
        'app': [
            'babel-polyfill',
            'react-hot-loader/patch',
            'webpack-hot-middleware/client?timeout=2000&overlay=false&reload=true',
            './react/src/index'
        ]
    },
    devServer: {
        stats: 'errors-only',
        hot: true,
        historyApiFallback: {
            index: '/react/public/index.html'
        }
    },
    output: {
        path: path.join(__dirname, './public/js'),
        filename: './js/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.css$/,
                include: /node_modules/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(gif|svg|jpg|png|jpeg|eot|woff|ttf|woff2)$/,
                loader: "file-loader",
                options: {
                    limit: 10000,
                    name: './images/[name].[ext]'
                },
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015','stage-0','react']
                        }
                    }
                ],
            },
            {
                test: /\.(js|jsx|mjs)$/,
                enforce: 'pre',
                exclude: /node_modules/,
                use: [
                    {
                        options: {
                            formatter: eslintFormatter,
                            eslintPath: require.resolve('eslint'),

                        },
                        loader: require.resolve('eslint-loader'),
                    },
                ],
                include: path.appSrc,
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ]
};
