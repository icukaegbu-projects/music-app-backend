const express = require('express');
const app = express.Router();

app.get('*.js', function (req, res, next) {
    if(!req.url.includes('service-worker.js')){
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
    }
    res.set('Content-Type', 'text/javascript');
    next();
});
app.get('*.css', function(req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/css');
    next();
});
module.exports = app;