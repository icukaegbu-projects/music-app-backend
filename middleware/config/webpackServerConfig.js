let {NODE_ENV,TITLE} =process.env;
let webpack = require('webpack'),
    webpackConfig = require('../../webpack.config'),
    compiler = webpack(webpackConfig),
    log = require('utils').logger.getLogger(TITLE),
    execFile = require('child_process').execFile;
module.exports = function (app) {
    if(NODE_ENV !== 'production'){
        app.use(require("webpack-dev-middleware")(compiler, {
            stats: 'errors-only'
        }));
        app.use(require("webpack-hot-middleware")(compiler,{
            log: false,
            path: '/__webpack_hmr',
            heartbeat: 10 * 1000
        }));
    }else {
        execFile('webpack', ['-p', '--config', './webpack.production.config.js'], (error, stdout, stderr) => {
            if (error) {
                log.error('Error building react in production mode ' + error);
                // throw error;
            }
            if(stderr){
                log.error('Error building react in production mode ' + error);
                // throw error;
            }
            log.info('Built react in production mode successfully ' + stdout);
        });

    }
};
