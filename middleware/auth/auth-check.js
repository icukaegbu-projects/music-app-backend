const jwt = require('jsonwebtoken');
const Users = require('mongoose').model('users');
const {KEY} = process.env;



/**
 *  The Auth Checker middleware function.
 */
export const isUser = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(401).end();
    }

    // get the last part from a authorization header string like "bearer token-value"
    const token = req.headers.authorization.split(' ')[1];

    // decode the token using a secret key-phrase
    return jwt.verify(token, KEY, (err, decoded) => {
        // the 401 code is for unauthorized status
        if (err) { return res.status(401).end(); }

        const userId = decoded.sub;

        // check if a Users exists
        return Users.findById(userId, (userErr, Users) => {
            if (userErr || !Users) {
                return res.status(401).end();
            }
            // pass Users details onto next route
            req.user = Users;
            return next();
        });
    });
};
export const isAdmin =(req,res,next)=>{
    if(req.user.acountType=== 'admin'){
        next();
    }
    else{
        return res.json({
            "success": false,
            "message": "Permission denied.",
            "errors": {}
        });

    }
};